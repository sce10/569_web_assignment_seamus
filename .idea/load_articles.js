$(document).ready(function () {
    var displayResources = $('#artcontain');
    displayResources.text('Loading Content...');
    $.ajax({
        url: 'https://sporadic.nz/2018a_web_assignment_service/Articles',
        // headers: {  'Access-Control-Allow-Origin': 'https://sporadic.nz/2018a_web_assignment_service/Articles' },
        type: 'GET',
        success: function(msg) {
            console.log("Received: " + JSON.stringify(msg));
                   let output="";


                   for (var i in msg)
                   {
                       output+='<div class="col-lg-6 col-mb-1">\n' +
                           '            <div class="card" id="' + msg[i].id + '">\n' +
                           '                <div class="card-header bg-primary text-white">' + msg[i].title + '</div>\n' +
                           '                <div class="card-body text-info text-black">\n' +
                           '\n' +
                           '                    <img class="img-fluid" src="' + msg[i].imageUrl + '" style=" border-radius: 10px; float: left; margin-right: 20px; margin-bottom: 20px">\n' +
                           '                    <p class="card-text" style="font-size: small;">' + msg[i].content + '</p>\n' +
                           '                </div>\n' +
                           '            </div>\n' +
                           '            </div>'
                       // output+='<div articleid="' + msg[i].id + '" authorid="' + msg[i].author_id + '" class="card pgcertArticle"><h6 class="card-header">' + msg[i].title + '</h6><div class="card-body"><p class="card-text" class="fullcontent">' + msg[i].content + '</p></div><div class="card-footer text-center bg-info fullercontent">Show full content</div></div><br>';
                   }

                   output+="";

                   displayResources.html(output);
        }
    });
});
